package com.solveapuzzle.mapping.agnostic;

public interface MappingConfiguration {

	
	public String getMappingIdentifer();
	
	public String getMappingType();
}
